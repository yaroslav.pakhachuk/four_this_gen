import axios from "axios";

export const AdminApi = {
  requestAllActiveUsers: async () => {
    try {
      const { data } = await axios.get(
        "https://boiling-wildwood-55780.herokuapp.com/users"
      );
      return data;
    } catch (err) {
      return err;
    }
  },
  deleteSelectedUsers: async (userIdArray, ownerId) => {
		if(userIdArray.length === 0) 
			return 'Please, select users you want to delete!'
    // const token = localStorage.getItem("token").split("\"")[1];
    // const instance = axios.create({
    //   baseURL: "https://boiling-wildwood-55780.herokuapp.com",
    //   headers: { Authorization: `Bearer ${token}` },
    // });
      
    try {
      const { data } = await Promise.all(
        userIdArray.map(async (user) => {
          return await axios.delete(
            `https://boiling-wildwood-55780.herokuapp.com/users/${user._id}`
          );
        })
      );
      return data;

    } catch (err) {
      return err;
    }
  },
};
