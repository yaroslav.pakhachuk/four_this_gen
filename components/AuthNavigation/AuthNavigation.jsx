import { useState, useEffect, useCallback } from "react";

import { useAuth } from "../../context";
import { NavLink } from "../../components";

import IconButton from "@mui/material/IconButton";
import MeetingRoomIcon from "@mui/icons-material/MeetingRoom";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import SensorDoorIcon from "@mui/icons-material/SensorDoor";

import s from "./AuthNavigation.module.scss";

export const AuthNavigation = () => {
  const { authUser, signOut } = useAuth();
  const [forceRerender, setRerender] = useState(false);
  const checkUserAuth = useCallback(() => (Boolean(authUser) ? true : false), [authUser]);

  useEffect(() => {
    setRerender(() => checkUserAuth());
  }, []);

  useEffect(() => {
    setRerender(() => checkUserAuth());
  }, [authUser]);

  return (
    <div className={s.btns}>
      {typeof window !== "undefined" && !!authUser && forceRerender ? (
        <>
          <NavLink href="/user">
            <IconButton className={s.btn}>
              <ManageAccountsIcon />
            </IconButton>
          </NavLink>
          <div onClick={signOut}>
            <IconButton className={s.btn}>
              <MeetingRoomIcon />
            </IconButton>
          </div>
        </>
      ) : (
        <>
          <NavLink href="/sign-up">
            <IconButton className={s.btn}>
              <PersonAddIcon />
            </IconButton>
          </NavLink>
          <NavLink href="/sign-in">
            <IconButton className={s.btn}>
              <SensorDoorIcon />
            </IconButton>
          </NavLink>
        </>
      )}
    </div>
    // <div className={s.btns}>
    //       <NavLink href="/user">
    //         <IconButton className={s.btn}>
    //           <ManageAccountsIcon />
    //         </IconButton>
    //       </NavLink>
    //       <div onClick={signOut}>
    //         <IconButton className={s.btn}>
    //           <MeetingRoomIcon />
    //         </IconButton>
    //       </div>
    //       <NavLink href="/sign-up">
    //         <IconButton className={s.btn}>
    //           <PersonAddIcon />
    //         </IconButton>
    //       </NavLink>
    //       <NavLink href="/sign-in">
    //         <IconButton className={s.btn}>
    //           <SensorDoorIcon />
    //         </IconButton>
    //       </NavLink>
    // </div>
  );
};
