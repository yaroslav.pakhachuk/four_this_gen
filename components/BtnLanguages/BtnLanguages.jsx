import {useState, useEffect} from 'react';
import PropTypes from "prop-types";
import { withTranslation } from "react-i18next";

import s from "./BtnLanguages.module.scss";

export const Langs = ({ i18n }) => {
  const [currentLang, setCurrentLang] = useState("en");
  
  const handleClick = () => {
    localStorage.setItem("locale", currentLang === "ua" ? "en" : "ua");
    setCurrentLang(i18n.language === "ua" ? "en" : "ua");
  }

  useEffect(() => {
    if(!localStorage.getItem('locale')) {
      localStorage.setItem("locale", currentLang === "ua" ? "en" : "ua");
    } else {
      setCurrentLang(localStorage.getItem('locale'));
    }
  }, []);

  useEffect(() => {
    i18n.changeLanguage(currentLang);
  }, [currentLang]);

  return (
    <div>
      <button onClick={handleClick} name={currentLang} className={s.btn}>
        {currentLang === "ua" ? "EN" : "UA"}
      </button>
    </div>
  );
}

Langs.propTypes = {
  i18n: PropTypes.object.isRequired,
};

export const BtnLanguages = withTranslation()(Langs)
