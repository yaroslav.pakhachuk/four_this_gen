import PropTypes from 'prop-types';
import cn from 'classnames'

import { Title } from "../../components";

import s from './CardStatistic.module.scss'

export const CardStatistic = ({ className, title, count, icon }) => {
    return (
      <div className={cn(s.card, className)}>
        <Title tag="h3" className={s.title}>
          {title}
        </Title>
        <Title tag="h4" className={s.subtitle}>
          <span className={s.icon}>{icon}</span> {count}
        </Title>
      </div>
    );
}

CardStatistic.propTypes = {
  title: PropTypes.string,
  count: PropTypes.number,
  icon: PropTypes.node,
  className: PropTypes.string,
};