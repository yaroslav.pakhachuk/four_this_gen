import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import axios from "axios";

import { useAuth, FavContext, CarContext } from "../../context";

import IconButton from "@mui/material/IconButton";
import Stack from "@mui/material/Stack";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import DeleteIcon from "@mui/icons-material/Delete";

import s from "./CardBtns.module.scss";

export const CardsButtons = ({ card }) => {
  const { error, refetch } = useContext(CarContext);
  const { authUser } = useAuth();
  const { currentLsState, toggleFav } = useContext(FavContext);

  const handlerDelete = async (e, cardId) => {
    e.preventDefault();
    try {
      const { data } = await axios.delete(
        `https://boiling-wildwood-55780.herokuapp.com/cars/${cardId}`
      );
      refetch();
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Stack direction="row" spacing={1}>
      <IconButton
        onClick={(e) => toggleFav(e, card)}
        aria-label="favourite"
        className={cn({
          [s.isFav]: currentLsState?.some((f) => f._id === card._id),
        })}
      >
        <FavoriteBorderIcon />
      </IconButton>
      {(authUser?.role === "super_admin" || authUser?._id === card.owner) && (
        <IconButton
          aria-label="delete"
          onClick={(e) => handlerDelete(e, card._id)}
        >
          <DeleteIcon />
        </IconButton>
      )}
    </Stack>
  );
};

CardsButtons.propTypes = {
  card: PropTypes.object,
};