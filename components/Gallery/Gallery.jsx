import { GalleryCard } from "../../components";
import PropTypes from 'prop-types';
import { withTranslation } from "react-i18next";

import s from "./Gallery.module.scss";

const Gal = ({refetch = () => {}, cars, search = "", searchWithAutocomplete = {}, selectedDate = null, t }) => {
  const filteredCars =
    search.length > 0
      ? [
          ...cars?.filter(
            (car) =>
              car.brand.toLowerCase().includes(search.toLowerCase()) ||
              car.model.toLowerCase().includes(search.toLowerCase())
          ),
        ]
      : searchWithAutocomplete?.brand && searchWithAutocomplete?.model
      ? [
          ...cars?.filter(
            (car) =>
              car.brand.toLowerCase().includes(searchWithAutocomplete.brand.toLowerCase()) &&
              car.model.toLowerCase().includes(searchWithAutocomplete.model.toLowerCase())
          ),
        ]
      :  cars
      ? [...cars]
      : null;

  return (
    <div className={s.gallery}>
      <>
        {typeof window !== "undefined" && filteredCars?.length
          ? filteredCars.map((card) => (
              <GalleryCard key={card._id} card={card} refetch={refetch} />
            ))
          : 'Not found'}
      </>
    </div>
  );
};
Gal.propTypes = {
  refetch: PropTypes.func, 
  cars: PropTypes.arrayOf(PropTypes.object), 
  search: PropTypes.string, 
  searchWithAutocomplete: PropTypes.object, 
  t: PropTypes.func
}
export const Gallery = withTranslation()(Gal);
