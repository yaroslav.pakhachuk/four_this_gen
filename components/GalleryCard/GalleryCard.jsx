import React, { memo } from "react";
import { format } from "date-fns";
import PropTypes from "prop-types";

import { CardsButtons } from "../../components";
import { NavLink } from "..";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

import cartemplateImg from "../../assets/images/carPage/defaultCar.jpeg";

import s from "./GalleryCard.module.scss";

// eslint-disable-next-line react/display-name
export const GalleryCard = memo(({ refetch = () => {}, isWesternHemisphere, card }) => {
  console.log("card renders", card._id);
  const avatarImg = card.avatar === undefined ? cartemplateImg.src : card.avatar;

  return (
    <Card sx={{ maxWidth: "33%" }} className={s.card}>
      <NavLink href={`/car/${card._id}`}>
        <CardActionArea>
          <CardMedia component="img" height="140" image={avatarImg} alt={`${card.brand} ${card.model}`} />
          <CardContent>
            <div className={s.dashboard}>
              <Typography className={s.text} gutterBottom variant="h5" component="div">
                {card.brand} {card.model}
                {/* {(card.brand + " " + card.model).length > 10
                  ? `${card.brand} ${card.model.slice(0, 7)}...`
                  : `${card.brand} ${card.model} `} */}
              </Typography>
              <CardsButtons card={card} refetch={refetch} />
            </div>
            <Typography gutterBottom variant="p" component="div">
              {isWesternHemisphere
                ? format(Date.parse(card.dateCreate), "mmm/dd/yyyy")
                : format(Date.parse(card.dateCreate), "dd MMM yyyy")}
            </Typography>
            <Typography className={s.text} variant="body2" color="text.secondary">
              {card.description}
            </Typography>
          </CardContent>
        </CardActionArea>
      </NavLink>
    </Card>
  );
});

GalleryCard.propTypes = {
  refetch: PropTypes.func,
  isWesternHemisphere: PropTypes.string,
  card: PropTypes.object,
};
