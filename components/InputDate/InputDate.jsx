import React from "react";
import PropTypes from "prop-types";

import TextField from "@mui/material/TextField";
import AdapterDateFns from "@material-ui/lab/AdapterDateFns";
import LocalizationProvider from "@material-ui/lab/LocalizationProvider";
import DatePicker from "@material-ui/lab/DatePicker";

export const InputDate = ({ label, value, setValue, min, max, className }) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        views={["year"]}
        label={label}
        value={value}
        minDate={min}
        maxDate={max}
        onChange={(newValue) => {
          const date = newValue.toISOString();
          setValue(date);
        }}
        inputProps={{ readOnly: true }}
        renderInput={(params) => (
          <TextField {...params} className={className} />
        )}
      />
    </LocalizationProvider>
  );
};

InputDate.defaultProps = {
  min: null,
};

InputDate.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  setValue: PropTypes.func.isRequired,
  className: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
};
