import React, { useState, useEffect, useContext } from "react";
import { withTranslation } from "react-i18next";
import axios from "axios";
import PropTypes from 'prop-types';

import { InputDate } from "../InputDate/InputDate";
import { useAuth, CarContext } from "../../context";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";

import m from "./Modal.module.scss";


const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  width: 500,
  border: "2px solid #000",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4,
};

const BasicModal = ({ t }) => {
  const [description, setDescription] = useState("");
  const [model, setModel] = useState("");
  const [year, setYear] = useState(0);
  const [brand, setBrand] = useState("");
  const { authUser } = useAuth();
  const { error, refetch } = useContext(CarContext);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.post(
        `https://boiling-wildwood-55780.herokuapp.com/cars?userId=${authUser._id}`,
        { brand, model, year: Number.parseInt(year), description }
      );
      refetch();
      handleClose();
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <div className={m.modal}>
      {authUser && Object.keys(authUser).length > 0 && (
        <Button className={m.btnCreate} onClick={handleOpen}>
          {t("createPostBtn")}
        </Button>
      )}

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Create post of your car
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2, marginBottom: 3 }}
          >
            Try to describe your car in those parts:
          </Typography>

          <form className={m.wrapper}>
            <TextField
              className={m.textField}
              id="brand"
              style={{ color: "primary" }}
              label={t("enter brand your car")}
              variant="outlined"
              type="text"
              onChange={(e) => setBrand(e.target.value)}
            />
            <TextField
              className={m.textField}
              id="model"
              label={t("enter model")}
              variant="outlined"
              type="text"
              onChange={(e) => setModel(e.target.value)}
            />
            <InputDate
              label={t("pickDateLabel")}
              value={year}
              setValue={(newValue) => {
                setYear(newValue);
              }}
              className={m.textField}
            />
            <TextField
              className={m.textField}
              sx={{ color: "#fff" }}
              id="model"
              label={t("enter description")}
              variant="outlined"
              type="text"
              onChange={(e) => setDescription(e.target.value)}
            />

            <div>
              <Button
                onClick={handleSubmit}
                variant="contained"
                className={m.btnCreate}
              >
                {t("create")}
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
    </div>
  );
};

BasicModal.propTypes = {
  t: PropTypes.func,
};

export const ModalCreate = withTranslation()(BasicModal)
