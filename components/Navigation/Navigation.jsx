import React from "react";
import { withTranslation } from "react-i18next";
import PropTypes from 'prop-types';

import { NavLink } from '../../components';
import { useAuth } from "../../context";
import { Typography } from "../Typography/Typography";

import HomeIcon from "@mui/icons-material/Home";
import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";
import TimelineIcon from "@mui/icons-material/Timeline";

import s from './Navigation.module.scss'

const Nav = ({ t }) => {
  const { authUser } = useAuth();

    return (
      <div className={s.navigation}>
        <NavLink href="/" exact className={s.link} active={s.activeLink}>
          <HomeIcon />
          <Typography tag="p" className={s.linkName}>
            {t("homeBtn")}
          </Typography>
        </NavLink>
        <NavLink href="/mycollection" className={s.link} active={s.activeLink}>
          <DirectionsCarIcon />
          <Typography tag="p" className={s.linkName}>
            {t("myCollectionBtn")}
          </Typography>
        </NavLink>
        {authUser?.role === "super_admin" && (
          <NavLink href="/mystatistic" className={s.link} active={s.activeLink}>
            <TimelineIcon />
            <Typography tag="p" className={s.linkName}>
              {t("myStatisticBtn")}
            </Typography>
          </NavLink>
        )}
      </div>
    );
}

Nav.defaultProps = {
  t: () => {},
};
Nav.propTypes = {
  t: PropTypes.func,
};



export const Navigation = withTranslation()(Nav);


