
import React from 'react';
import PropTypes from 'prop-types';
import cn from "classnames";

import s from './Title.module.css';


export function Title({ tag, children, className, ...restProps }) {
	switch (tag) {
		case 'h1':
			return <h1 className={cn(s.title, className)} {...restProps}>{children}</h1>;
		case 'h2':
			return <h2 className={cn(s.title, className)} {...restProps}>{children}</h2>;
		case 'h3':
			return <h3 className={cn(s.title, className)} {...restProps}>{children}</h3>;
		case 'h4':
			return <h4 className={cn(s.title, className)} {...restProps}>{children}</h4>;
		default: return <></>;
	}
};

Title.propTypes = {
  tag: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  className: PropTypes.string,
};