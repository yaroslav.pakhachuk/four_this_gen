import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import s from "./Typography.module.css";

export function Typography({ tag, children, className, ...restProps }) {
  switch (tag) {
    case "p":
      return (
        <p className={cn(s.p, className)} {...restProps}>
          {children}
        </p>
      );
    case "span":
      return (
        <span className={cn(s.span, className)} {...restProps}>
          {children}
        </span>
      );
    default:
      return <></>;
  }
}

Typography.propTypes = {
  tag: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  className: PropTypes.string,
};
