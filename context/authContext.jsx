// import { useHistory } from "react-router-dom";
import { createContext, useContext, useState, useEffect } from "react";
import { useLocalStorage } from "react-use";
import { push } from "next/router";
import axios from "axios";
import { decode } from "jsonwebtoken";

const isExpiredToken = (token) => {
  const { exp } = decode(token);
  return exp * 1000 - Date.now() <= 0;
};

export const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export const AuthProvider = ({ children }) => {
  const [token, setToken, removeToken] = useLocalStorage("token");
  const [authUser, setAuthUser, removeAuthUser] = useLocalStorage("user");

  if (token) {
    if (isExpiredToken(token)) {
      removeToken();
      removeUser();
      push("http://localhost:3000/sign-in");
    } else {
      if (authUser) {
        const serialRole = JSON.stringify(authUser.role);
        localStorage.setItem("role", serialRole);
      }
      axios.defaults.headers.Authorization = `Bearer ${token}`;
    }
  }

  const updateToken = (newToken) => {
    axios.defaults.headers.Authorization = `Bearer ${token}`;
    setToken(newToken);
  };

  const signIn = ({ user, accessToken }) => {
    localStorage.setItem("role", JSON.stringify(""));
    if (!accessToken) {
      return push("http://localhost:3000/sign-in");
    }
    setAuthUser(user);
    updateToken(accessToken);
  };

  // const register = () => {};

  const signOut = () => {
    setAuthUser(null);
    localStorage.setItem("role", JSON.stringify(""));
    removeToken();
    removeAuthUser();
    axios.defaults.headers.Authorization = "";
  };
  
  return (
    <AuthContext.Provider
      value={{
        authUser,
        signIn,
        signOut,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
