import { useState, useEffect, createContext, useCallback } from "react";
import axios from "axios";
export const CarContext = createContext();

export const CarsDataProvider = ({children}) => {
  const [carsData, setCarsData] = useState(null);
  const [error, setError] = useState('');

  const refetch = useCallback(async () => {
    try {
      const { data } = await axios.get(`https://boiling-wildwood-55780.herokuapp.com/cars`);
      setCarsData(data?.cars?.data);
    } catch (err) {
      setError(err);
    }
  }, []);
  // useEffect(() => {
  //   console.log("carsData: ", carsData);
  // }, [carsData])
  

  return (
    <CarContext.Provider value={{ carsData, refetch, error }}>
      {children}
    </CarContext.Provider>
  );
};


