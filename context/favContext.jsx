import { createContext, useState, useEffect } from "react";

export const FavContext = createContext();

export const FavProvider = ({ children }) => {
  const [currentLsState, setCurrentLsState] = useState([])

  useEffect(() => {
    if(!localStorage.getItem('favCarsArray')){
      localStorage.setItem('favCarsArray', [])
      }else {
        setCurrentLsState(JSON.parse(localStorage.getItem('favCarsArray')))
      }
  },[])
  
    useEffect(() => {
      localStorage.setItem('favCarsArray', JSON.stringify(currentLsState) || [])
  },[currentLsState])

  const toggleFav = (e, card) => {
    e.stopPropagation();
    e.preventDefault();
    if(currentLsState.find(c => c._id === card._id)) {
      setCurrentLsState(currentLsState.filter(f => f._id != card._id))
    } else {
      setCurrentLsState([...JSON.parse(localStorage.getItem('favCarsArray')), card]);
    }
  }

  return (
    <FavContext.Provider
      value={
        {
          currentLsState,
          toggleFav,
        }
      }
    >
      {children}
    </FavContext.Provider>
  );
};

