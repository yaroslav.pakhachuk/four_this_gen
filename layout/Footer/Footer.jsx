import React, { ReactElement } from 'react'
import { Typography } from '../../components';
import s from './Footer.module.scss';
import { withTranslation } from 'react-i18next';

export function Footer({ t }) {
	return (
		<footer className={s.footer}>
			<div className={s.footerInner}>
				
				<Typography tag="p" className={s.rights}>
				{t('taskLimited')}
				</Typography>

			</div>
		</footer>
	)
}

export default withTranslation()(Footer);
