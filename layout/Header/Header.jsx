import React from "react";
import Image from "next/image";
import { Navigation, AuthNavigation, BtnLanguages, NavLink } from "../../components";
import imgLogo from "../../assets/images/logo.png";
import s from "./Header.module.scss";

export default function Header({}) {
  return (
    <header className={s.header}>
      <NavLink href="/" className={s.logo}>
        <Image src={imgLogo} layout="fixed" height="70px" width="150px" alt="4this.gen" />
      </NavLink>
      <Navigation />
      <div className={s.btns}>
        <AuthNavigation />
        <BtnLanguages />
      </div>
    </header>
  );
}
