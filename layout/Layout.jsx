import React, { useEffect } from 'react'
import Footer from './Footer/Footer'
import Header from './Header/Header'
import Main from './Main/Main'

import s from './Layout.module.css';
import {
  AuthProvider,
  CarsDataProvider,
  FavProvider,
} from "../context";


function Layout({ children }) {
	return (
		<div className={s.app}>
			<Header />
				<Main>
					{children}
				</Main>
			<Footer />
		</div>
	)
}


export const withLayout = (Component) => {
	// eslint-disable-next-line react/display-name
	return (props) => {
		return (
      <CarsDataProvider>
        <AuthProvider>
          <FavProvider>
            <Layout>
              <Component {...props} />
            </Layout>
          </FavProvider>
        </AuthProvider>
      </CarsDataProvider>
    );
	}
}