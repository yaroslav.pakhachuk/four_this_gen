import React from 'react'
import cn from 'classnames';
import s from './Main.module.scss';

export default function Main({ children, className }) {
	return (
		<main className={cn(s.main, className)}>
			{children}
		</main>
	)
}
