import React, {useContext} from "react";
import { withTranslation } from "react-i18next";
import Image from "next/image";
import Head from "next/head";
import {useRouter} from 'next/router'
import axios from "axios";
import cn from "classnames";

import { useAuth } from "../../context";
import { Title, Typography } from "../../components";
import { withLayout } from "../../layout/Layout";
import { FavContext } from "../../context";

import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';

import cartemplateImg from "../../assets/images/carPage/defaultCar.jpeg";

import s from "../../styles/Car.module.scss";

const Car = (props) => {
  const {
    currentLsState,
    toggleFav,
  } = useContext(FavContext);
  const { authUser } = useAuth();
  const router = useRouter();

  const handlerDelete = async (e, cardId) => {
    e.preventDefault();
    try {
        const { data } = await axios.delete(`https://boiling-wildwood-55780.herokuapp.com/cars/${cardId}`);
        router.push('/')
    } catch (err) {
      console.log(err);
    }
};

  return (
    <div className={s.carContainer}>
      <Head>
        <title>{props?.data?.carById?.brand + " " + props?.data?.carById?.model}</title>
        <meta name="description" content={props?.data?.carById?.description} />
        <meta property="og:title" content={props?.data?.carById?.brand + " " + props?.data?.carById?.model} />
        <meta property="og:description" content={props?.data?.carById?.description} />
        <meta property="og:type" content="article" />
      </Head>
      <div className={s.cardImage}>
        <Image
          src={props?.data?.carById?.avatar || cartemplateImg}
          layout="intrinsic"
          objectFit={"cover"}
          width={900}
          height={350}
          alt={"car"}
          quality={100}
        />
      </div>
      <div className={s.cardContent}>
        <Title tag="h1" className={s.title}>
          {props.t("car")} {props?.data?.carById?.brand + " " + props?.data?.carById?.model}
        </Title>
        <div className={s.infosWrapper}>
          <div className={s.infoLine}>
            <Typography tag="span" className={s.infoName}>
              {props.t("brand")}
            </Typography>
            <div className={s.devider} />
            <Typography tag="span" className={s.infoDescription}>
              {props?.data?.carById?.brand}
            </Typography>
          </div>
          <div className={s.infoLine}>
            <Typography tag="span" className={s.infoName}>
              {props.t("model")}
            </Typography>
            <div className={s.devider} />
            <Typography tag="span" className={s.infoDescription}>
              {props?.data?.carById?.model}
            </Typography>
          </div>
          <div className={s.infoLine}>
            <Typography tag="span" className={s.infoName}>
              {props.t("year")}
            </Typography>
            <div className={s.devider} />
            <Typography tag="span" className={s.infoDescription}>
              {props?.data?.carById?.year}
            </Typography>
          </div>
          <div className={s.infoLine}>
            <Typography tag="span" className={s.infoName}>
              {props.t("dateOfCreation")}
            </Typography>
            <div className={s.devider} />
            <Typography tag="span" className={s.infoDescription}>
              {props?.data?.carById?.dateCreate}
            </Typography>
          </div>
        </div>
        <Title className={s.infoTextTitle} tag="h2">
          {props.t("description")}
        </Title>
        <Typography tag="p" className={s.infoText}>
          {props?.data?.carById?.description}
        </Typography>
        <div className={s.handlingButtons}>
          <div className={s.uploadWrapper}>
            <p className={s.uploadText}>{props.t("uploadImage")}</p>
            <input className={s.uploadInput} type="file" />
          </div>
          <button
            onClick={(e) => toggleFav(e, props?.data?.carById)}
            className={cn(s.btnFavourite, {
              [s.isFav]: currentLsState?.some((f) => f._id === props?.data?.carById._id),
            })}
          >
            <FavoriteBorderIcon />
          </button>
          {authUser && Object.keys(authUser).length > 0 && (
            <>
              <button className={s.btnEdit}>
                <EditIcon />
              </button>
              <button onClick={(e) => handlerDelete(e, props?.data?.carById)} className={s.btnDelete}>
                <DeleteIcon />
              </button>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export async function getStaticPaths() {
  try {
    const { data } = await axios.get(`https://boiling-wildwood-55780.herokuapp.com/cars`);
    const paths = data?.cars?.data?.map((car) => ({
          params: { id: car._id },
    }))

    return { paths, fallback: false };
  } catch (e) {
    console.log(e);
  }
  // Get the paths we want to pre-render based on posts, play with params variable you are returning

}
export const getStaticProps = async ({params}) => {
  const cardId = params.id;

  try {
    const { data } = await axios.get(`https://boiling-wildwood-55780.herokuapp.com/cars/${cardId}`)
    return {
      props: { data },
    };
  } catch (e) {
    return {
      props: { data: null },
    };
  }

};



export default withTranslation()(withLayout(Car));
