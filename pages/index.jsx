import { useState, useEffect, useContext } from "react";
import { withTranslation } from "react-i18next";
import { useAsync } from "react-use";
import Head from "next/head";
import moment from "moment";
import axios from "axios";

import { Gallery } from "../components";
import { CarContext } from "../context";
import { withLayout } from "../layout/Layout";
import { ModalCreate } from "../components/Modal/Modal";
import Pagination from "@mui/material/Pagination";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import s from "../styles/Home.module.css";

const Home = (props) => {
  const { error, refetch, carsData } = useContext(CarContext);
  const [sortByDate, setSortByDate] = useState();
  const [cars, setCars] = useState(carsData ?? props?.data?.cars.data);
  const [resetFilter, setResetFilter] = useState(false);
  const [search, setSearch] = useState("");
  const [searchWithAutocomplete, setSearchWithAutocomplete] = useState({});
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(1);
  const handleChange = (event, value) => {
    setPage(value);
  };

  useEffect(() => {
    if (carsData) {
      setCars(carsData);
    }
  }, [carsData]);

  useEffect(() => {
    setSearchWithAutocomplete({});
  }, [search]);
  useEffect(() => {
    setResetFilter(false);
  }, [sortByDate]);

  useEffect(() => {
    setSortByDate();
  }, [resetFilter]);

  useAsync(async () => {
    try {
      const { data } = await axios.get(`https://boiling-wildwood-55780.herokuapp.com/cars?page=${page}`);
      setCars(data.cars.data);
      setPages(data.cars.pageCount);
    } catch (error) {
      console.log(error);
    }
  }, [page]);

  const serchHints = cars.map((card) => ({
    ...card,
    label: card?.brand + " " + card?.model,
  }));

  return (
    <div className={s.container}>
      <Head>
        <title>4this.gen</title>
        <meta name="description" content="Car gallery" />
        <meta property="og:title" content="Car gallery" />
        <meta property="og:description" content="Just watch and relax!" />
        <meta property="og:type" content="article" />
      </Head>
      <div className={s.dashboard}>
        <div className={s.searchBar}>
          <Autocomplete
            id="search"
            options={serchHints}
            blurOnSelect
            onChange={(e, newValue) =>
              setSearchWithAutocomplete({
                brand: newValue?.brand,
                model: newValue?.model,
              })
            }
            renderInput={(params) => (
              <TextField
                {...params}
                label={props.t("search")}
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            )}
          />
        </div>
        <ModalCreate />
      </div>
      <div className={s.gallery}>
        <Gallery
          search={search}
          refetch={refetch}
          searchWithAutocomplete={searchWithAutocomplete}
          cars={cars}
          selectedDate={moment(sortByDate).format("YYYY")}
        />
        <Pagination count={pages} page={page} onChange={handleChange} className={s.pagination} color="primary"/>
      </div>
    </div>
  );
};

export const getStaticProps = async () => {
  try {
    const { data } = await axios.get(`https://boiling-wildwood-55780.herokuapp.com/cars`);
    if (!data?.cars?.data?.length) {
      return {
        props: { data: null },
      };
    }
    return {
      props: { data },
    };
  } catch (e) {
    return {
      props: { data: null },
    };
  }
};

export default withTranslation()(withLayout(Home));
