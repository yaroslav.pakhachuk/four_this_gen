import React, {useState, useEffect} from 'react';
import { withTranslation } from "react-i18next";
import Head from 'next/head';
import axios from "axios";

import { withLayout } from "../layout/Layout";
import { Gallery } from '../components';

import s from "../styles/Home.module.css";


const MyCollection = (props) => {
  const [favCards, setFavCards] = useState([]);

  useEffect(() => {
    if(localStorage.getItem('favCarsArray')?.length) {
      setFavCards(JSON.parse(localStorage.getItem('favCarsArray')))
    }
  }, [])

  return (<>
    <Head>
      <title>Улюблені автомобілі</title>
    </Head>
    <div className={s.gallery}>
      {favCards.length ? <Gallery cars={favCards} /> : <p className={s.notFound}>{props.t('myCollectionMessage')}</p>}
    </div>
    </>
  );
}


export default withTranslation()(withLayout(MyCollection));

export const getStaticProps = async () => {
  try {
    const { data } = await axios.get(`https://boiling-wildwood-55780.herokuapp.com/cars`);
    if (!data?.cars?.data.length) {
      return {
        props: { data: null },
      };
    }
    return {
      props: { data },
    };
  } catch(e) {
    return {
      props: { data: null },
    };
  }
};
