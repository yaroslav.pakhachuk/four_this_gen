import { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import axios from "axios";


import { AdminApi } from "../api/adminAPI";
import { withLayout } from "../layout/Layout";
import { CardStatistic, Title } from "../components";
import { useAuth } from "../context";

import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Checkbox from "@mui/material/Checkbox";
import Avatar from "@mui/material/Avatar";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";

import s from "../styles/MyStatistic.module.scss";

export const MyStatistic = (props) => {
  const [statisticUsers, setStatisticUsers] = useState();
  const { authUser } = useAuth();
  const [usersArray, setUsersArray] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const [selectedUsers, setSelectedUsers] = useState([]);

  useEffect(() => {
    console.log("selectedUsers: ", selectedUsers);
  }, [selectedUsers]);

  const refetchAllStatistic = async () => {
    setIsFetching(true);
    setLoading(true);
    try {
      const users = await AdminApi.requestAllActiveUsers();
      // const { data } = await axios.get(
      //   `https://boiling-wildwood-55780.herokuapp.com/users`
      // );

      setStatisticUsers(users?.users?.length);
      setUsersArray(users?.users);
    } catch (err) {
      console.error(err);
    }
    setLoading(false);
    setIsFetching(false);
  };

  useEffect(() => {
    refetchAllStatistic();
  }, []);

  const handleToggle = (user) => () => {
    if (selectedUsers.some((u) => u._id === user._id)) {
      setSelectedUsers(selectedUsers.filter((u) => u._id !== user._id));
    } else {
      setSelectedUsers([...selectedUsers, user]);
    }
  };

  const handleDeleteSelectedUsers = async () => {
    setIsFetching(true);
    try {
      const result = await AdminApi.deleteSelectedUsers(selectedUsers);
      console.log('result: ', result);
    } catch (e) {
      console.error(e);
    }
    refetchAllStatistic();
    setIsFetching(false);
  };

  return (
    <div className={s.adminContainer}>
      <Title tag="h1" className={s.titleUsers}>
        General statistic
      </Title>
      <div className={s.box}>
        {statisticUsers && props.data.cars.data.length && (
          <>
            <CardStatistic
              title={props.t("carsStatisticTitle")}
              className={s.card}
              count={props.data.cars.data.length}
              icon={<DirectionsCarIcon fontSize="large" />}
            />
            <CardStatistic
              className={s.card}
              title={props.t("userStatisticTitle")}
              count={statisticUsers}
              icon={<PeopleAltIcon fontSize="large" />}
            />
          </>
        )}
      </div>
      <div className={s.usersContainer}>
        <Title tag="h1" className={s.titleUsers}>
          Active users{" "}
          <button
            onClick={handleDeleteSelectedUsers}
            className={s.btnDeleteUsers}
            disabled={isFetching}
          >
            Delete selected users
          </button>
        </Title>
        {!isLoading && (
          <List
            dense
            sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
            className={s.list}
          >
            {usersArray?.map((user) => {
              const labelId = `checkbox-list-secondary-label-${user._id}`;
              return (
                <ListItem
                  key={user._id}
                  secondaryAction={
                    <Checkbox
                      edge="end"
                      onChange={handleToggle(user)}
                      checked={selectedUsers.some((u) => u._id === user._id)}
                      inputProps={{ "aria-labelledby": user._id }}
                    />
                  }
                  disablePadding
                >
                  <ListItemButton>
                    <ListItemAvatar>
                      <Avatar alt={`Avatar n°${user._id}`} src={user.avatar} />
                    </ListItemAvatar>
                    <ListItemText id={labelId} primary={user.name} />
                  </ListItemButton>
                </ListItem>
              );
            })}
          </List>
        )}
      </div>
    </div>
  );
};

export const getStaticProps = async () => {
  try {
    const { data } = await axios.get(
      `https://boiling-wildwood-55780.herokuapp.com/cars`
    );
    if (!data?.cars?.data?.length) {
      return {
        props: { data: null },
      };
    }
    return {
      props: { data },
    };
  } catch (e) {
    return {
      props: { data: null },
    };
  }
};

export default withTranslation()(withLayout(MyStatistic));
