import { withTranslation } from "react-i18next";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import * as yup from "yup";
import axios from "axios";

import { withLayout } from "../layout/Layout";
import { useAuth } from "../context";
import { NavLink, Title } from "../components";

import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

import s from "../styles/SignIn.module.scss";

export const SignIn = ({ t }) => {
  const { signIn } = useAuth();
  const router = useRouter();
  const f = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: yup.object({
      email: yup.string().email(t("emailValidation")).required(t("requiredField")),
      password: yup
        .string()
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,128})/, {
          message: t("passwordValidation"),
        })
        .required(t("required")),
    }),
    onSubmit: async (values) => {
      try {
        const { data } = await axios.post(`https://boiling-wildwood-55780.herokuapp.com/signin`, values);
        await signIn(data);
        router.push('/')
      } catch ({ response: { data } }) {
        setRegistrationMessage(data.message);
      }
    },
  });

  return (
    <form className={s.wrapper_signIn} onSubmit={f.handleSubmit}>
      <Title tag="h1" className={s.title}>
        {t("signIn")}
      </Title>
      <TextField
        error={f.errors?.email && f.touched.email ? true : false}
        fullWidth
        id="email"
        name="email"
        label={t("enterEmail")}
        value={f.values.email}
        onBlur={f.handleBlur}
        onChange={f.handleChange}
        className={s.textField}
        helperText={f.errors?.email && f.touched.email ? f.errors?.email : " "}
      />
      <TextField
        error={f.errors?.password && f.touched.password ? true : false}
        fullWidth
        id="password"
        name="password"
        type="password"
        label={t("enterPassword")}
        value={f.values.password}
        onBlur={f.handleBlur}
        onChange={f.handleChange}
        className={s.textField}
        helperText={f.errors?.password && f.touched.password ? f.errors?.password : " "}
      />
      <div>
        <Button variant="contained" className={s.btnSignIn} type="submit">
          {t("signIn")}
        </Button>
      </div>
      <div>
        {t("dontHaveAccount")}{" "}
        <NavLink href="/sign-up" className={s.singLink}>
          {t("signUp")}.
        </NavLink>
      </div>
    </form>
  );
};

export default withTranslation()(withLayout(SignIn));
