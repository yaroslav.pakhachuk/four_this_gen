import { useState } from "react";
import { withTranslation } from "react-i18next";
import { useFormik } from "formik";
import * as yup from "yup";
import axios from "axios";

import { withLayout } from "../layout/Layout";
import { NavLink, Title } from "../components";

import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

import s from "../styles/SignUp.module.scss";

export const SignUp = ({ t }) => {
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [registrationMessage, setRegistrationMessage] = useState("");

  const f = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
    },
    validationSchema: yup.object({
      name: yup
        .string()
        .matches(/^[A-Z][a-z]+\s[A-Z][a-z]+$/, {
          message: t("nameValidation"),
        })
        .required(t("requiredField")),
      email: yup.string().email(t("emailValidation")).required(t("requiredField")),
      password: yup
        .string()
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,128})/, {
          message: t("passwordValidation"),
        })
        .required(t("required")),
    }),
    onSubmit: async (values) => {
      try {
        const res = await axios.post(`https://boiling-wildwood-55780.herokuapp.com/signup`, values);
        setRegistrationSuccess(true);
        setRegistrationMessage(res.data.message);
      } catch ({ response: { data } }) {
        setRegistrationMessage(data.message);
      }
    },
  });

  return (
    <div className={s.wrapper_signIn}>
      {registrationSuccess && registrationMessage ? (
        <Title tag="h4">{registrationMessage}</Title>
      ) : (
        <>
          {registrationMessage && <Title tag="h4" className={s.err}>{registrationMessage}</Title>}
          <form onSubmit={f.handleSubmit}>
            <Title tag="h1" className={s.title}>
              {t("signUp")}
            </Title>
            <TextField
              error={f.errors?.name && f.touched.name ? true : false}
              fullWidth
              id="name"
              name="name"
              label={t("enterName")}
              value={f.values.name}
              onBlur={f.handleBlur}
              onChange={f.handleChange}
              className={s.textField}
              helperText={f.errors?.name && f.touched.name ? f.errors?.name : " "}
            />
            <TextField
              error={f.errors?.email && f.touched.email ? true : false}
              fullWidth
              id="email"
              name="email"
              label={t("enterEmail")}
              value={f.values.email}
              onBlur={f.handleBlur}
              onChange={f.handleChange}
              className={s.textField}
              helperText={f.errors?.email && f.touched.email ? f.errors?.email : " "}
            />
            <TextField
              error={f.errors?.password && f.touched.password ? true : false}
              fullWidth
              id="password"
              name="password"
              type="password"
              label={t("enterPassword")}
              value={f.values.password}
              onBlur={f.handleBlur}
              onChange={f.handleChange}
              className={s.textField}
              helperText={f.errors?.password && f.touched.password ? f.errors?.password : " "}
            />
            <div>
              <Button variant="contained" className={s.btnSignUp} type="submit">
                {t("signUp")}
              </Button>
            </div>
            <div>
              {t("haveAccount")}{" "}
              <NavLink href="/sign-in" className={s.singLink}>
                {t("signIn")}.
              </NavLink>
            </div>
          </form>
        </>
      )}
    </div>
  );
};

export default withTranslation()(withLayout(SignUp));
