import React from "react";
import { withTranslation } from "react-i18next";
import Head from "next/head";
import { useRouter } from "next/router";
import axios from "axios";

import { withLayout } from "../layout/Layout";
import { Title, Typography } from "../components";
import { useAuth } from "../context";

import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Button } from "@mui/material";

import s from "../styles/User.module.scss";

const User = ({ data, t }) => {
  const { authUser } = useAuth();
  const router = useRouter();

  const handlerUpload = async (e, userId) => {
    const file = e.target.files[0];
    e.preventDefault();
    try {
      const fd = new FormData();
      fd.append("name", file.name);
      fd.append("avatar", file);

      await axios.patch(
        `https://boiling-wildwood-55780.herokuapp.com/users/${userId}/avatars`,
        fd
      );
      router.push("/user");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className={s.userContainer}>
      <Head>
        <title>User profile</title>
        {/* <meta name="description" content="Ohh, shit! Here we go again!" />
        <meta property="og:title" content="Test Task" />
        <meta property="og:description" content="Just click and relax!" />
        <meta property="og:type" content="article" /> */}
      </Head>
      <Title tag="h1" className={s.title}>
        {t("welcome")}, {authUser?.name}
      </Title>
      <div className={s.userContainerInner}>
        <div className={s.userAvatar}>
          {!authUser?.avatar ? (
            <AccountCircleIcon />
          ) : (
            <img src={authUser?.avatar} />
          )}
        </div>
        <div className={s.userData}>
          <Typography tag="p" className={s.userField}>
            {authUser?.name}
          </Typography>
          <Typography tag="p" className={s.userField}>
            {authUser?.email}
          </Typography>
          <input
            accept="image/*"
            style={{ display: "none" }}
            id="raised-button-file"
            type="file"
            name="avatar"
            onChange={(e) => handlerUpload(e, authUser._id)}
          />
          <label htmlFor="raised-button-file">
            <Button
              variant="contained"
              component="span"
              className={s.btnUpload}
            >
              {t("uploadAvatar")}
            </Button>
          </label>
        </div>
      </div>
      <div className={s.devider} />
    </div>
  );
};

export default withTranslation()(withLayout(User));
