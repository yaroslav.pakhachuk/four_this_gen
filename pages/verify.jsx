import { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { useRouter } from "next/router";
import axios from "axios";

import { withLayout } from "../layout/Layout";
import { Title } from "../components";

import s from "../styles/SignUp.module.scss";

export const Verify = ({ t }) => {
  const { asPath } = useRouter();
  const [message, setMessage] = useState();

  const verify = async (token) => {
    try {
      const params = { token };
      const { data } = await axios.get(
        `https://boiling-wildwood-55780.herokuapp.com/verify`,
        { params }
      );
      setMessage(data.message);
    } catch ({ response: { data } }) {
      setMessage(data.message);
    }
  };

  useEffect(() => {
    if (asPath.includes("token")) {
      const [_, token] = asPath.split("=");
      verify(token);
    }
  }, []);


  return <div className={s.wrapper_signIn}>{message ? <Title tag="h4">{message}</Title> : <div>Loading...</div>}</div>;
};

export default withTranslation()(withLayout(Verify));
